import click
import pyrealsense2 as rs
import numpy as np
import math as m
import tensorflow as tf

import cv2
import matplotlib.pyplot as plt

import efficientdet


@click.command()
# @click.option('--image', type=click.Path(dir_okay=False, exists=True))
@click.option('--checkpoint', type=click.Path())
@click.option('--efficientdet', type=int, default=0,
              help='EfficientDet architecture. '
                   '{0, 1, 2, 3, 4, 5, 6, 7}')
@click.option('--bidirectional/--no-bidirectional', default=True,
              help='If bidirectional is set to false the NN will behave as '
                   'a "normal" retinanet, otherwise as EfficientDet')
@click.option('--format', type=click.Choice(['VOC', 'labelme']),
              required=True, help='Dataset to use for training')
@click.option('--classes-names',
              default='', type=str,
              help='Only required when format is labelme. '
                   'Name of classes separated using comma. '
                   'class1,class2,class3')
def main(**kwargs):
    if kwargs['format'] == 'labelme':
        classes = kwargs['classes_names'].split(',')
        class2idx = {c: i for i, c in enumerate(classes)}
        n_classes = len(classes)

    elif kwargs['format'] == 'VOC':
        class2idx = efficientdet.data.voc.LABEL_2_IDX
        classes = efficientdet.data.voc.IDX_2_LABEL
        n_classes = 6

    # Load model
    model = efficientdet.EfficientDet(
        num_classes=n_classes,
        D=kwargs['efficientdet'],
        bidirectional=kwargs['bidirectional'],
        freeze_backbone=True,
        weights=None)

    model.load_weights(kwargs['checkpoint'])

    # load image
    im_size = model.config.input_size

    # D354

    pipeline_1 = rs.pipeline()
    config_1 = rs.config()
    config_1.enable_device('831612072177')
    config_1.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config_1.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    # Start streaming from both cameras
    profile = pipeline_1.start(config_1)

    sensor_dep = profile.get_device().first_depth_sensor()
    # sensor_dep.set_option(rs.option.exposure, 165000)
    sensor_dep.set_option(rs.option.emitter_enabled, 1)
    sensor_dep.set_option(rs.option.laser_power, 360)

    align_to = rs.stream.color
    alignedFs = rs.align(align_to)

    try:
        for _ in range(500):
            # Camera D435
            # Wait for a coherent pair of frames: depth and color
            frames_1 = pipeline_1.wait_for_frames()
            depth_frame_1 = frames_1.get_depth_frame()
            color_frame_1 = frames_1.get_color_frame()
            if not depth_frame_1 or not color_frame_1:
                continue
            # Convert images to numpy arrays
            # depth_image_1 = np.asanyarray(depth_frame_1.get_data())
            color_image_1 = np.asanyarray(color_frame_1.get_data())
            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            # depth_colormap_1 = cv2.applyColorMap(cv2.convertScaleAbs(depth_image_1, alpha=0.5), cv2.COLORMAP_JET)
            im = tf.image.convert_image_dtype(color_image_1, tf.float32)
            im = tf.image.resize(im, (im_size,) * 2)
            # im = efficientdet.utils.io.load_image(color_image_1, (im_size,) * 2)
            norm_image = efficientdet.data.preprocess.normalize_image(im)

            boxes, labels, scores = model(tf.expand_dims(norm_image, axis=0),
                                          training=False)

            labels = [classes[l] for l in labels[0]]

            im = im.numpy()
            for l, box, s in zip(labels, boxes[0].numpy(), scores[0]):
                x1, y1, x2, y2 = box.astype('int32')

                cv2.rectangle(im,
                              (x1, y1), (x2, y2), (0, 255, 0), 2)
                cv2.putText(im, l + ' {:.2f}'.format(s),
                            (x1, y1 - 10),
                            cv2.FONT_HERSHEY_PLAIN,
                            2, (0, 255, 0), 2)
            # plt.imshow(im)
            # plt.show(block=True)

            cv2.imshow("d435", im)

            # height_original, weight_original = colorized_depth.shape[0], colorized_depth.shape[1]
            # center_original_x, center_original_y = int(weight_original / 2), int(height_original / 2)
            # angle_per_px_x, angle_per_px_y = 42.5 / weight_original, 69.4 / height_original

            # time.sleep(0.1)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                break

    finally:
        # Stop streaming
        pipeline_1.stop()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
